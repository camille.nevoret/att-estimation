
ATT_estimate_fct2 = function(table_i,nb_covar,nb_covar_bsln,pas,B_true,sigma_true,B_hat,sigma_hat){
  #-----------------------------------------------------------------------------------------------#
  # ARGUMENTS :                                                                                   #
  # table_i : dataframe, one line per patient per time interval                                   #
  # nb_covar : integer, number of time-varying covariates                                         #
  # nb_covar_bsln : integer, number of baseline covariates                                        #
  # pas : float, maximum length of time interval                                                  #
  # B_true : matrix nb_covar * nb_covar, known parameter of the VAR model                         #
  # sigma_true : float, known parameter of the VAR model                                          #
  # B_hat : matrix nb_covar * nb_covar, estimated parameter of the VAR model                      #
  # sigma_hat : float, estimated parameter of the VAR model                                       #
  #                                                                                               #
  # VALUES :                                                                                      #
  # coefficients : ATT estimate without correction                                                #
  # coefficients_new : ATT estimate with correction using estimated parameters of the VAR model   #
  # coefficients_new_true : ATT estimate with correction using known parameters of the VAR model  #
  #-----------------------------------------------------------------------------------------------#
  

  coefficients = matrix(0,nrow =length(seq(0,max(table_i$time2)-1,by=pas)),ncol=2+nb_covar+nb_covar_bsln) 
  coefficients_new = matrix(0,nrow =length(seq(0,max(table_i$time2)-1,by=pas)),ncol=2+nb_covar+nb_covar_bsln)
  coefficients_new_true = matrix(0,nrow =length(seq(0,max(table_i$time2)-1,by=pas)),ncol=2+nb_covar+nb_covar_bsln)

  c = 1
  for (interval in round(seq(0,max(table_i$time2)-1,by=pas),3)){
    correct = matrix(0,ncol = nb_covar,nrow =nb_covar)
    correct_true = matrix(0,ncol = nb_covar,nrow =nb_covar)
    table_i_interval = filter(table_i,time1-interval >  -1e-14 & time1-interval < 1e-14 & time2 <= interval+pas +1e-14)
    cov_interval = as.matrix(cbind(rep(1,nrow(table_i_interval)),
                                   table_i_interval[,c("B",paste0("Z",1:nb_covar_bsln),paste0("L",1:nb_covar,"_s_new"))]))####
    
    # Creation of a table with the number of events in the time interval of length "step"
    table_i_interval_E = filter(table_i,time1>=interval - 1e-14 & time2 <= interval +pas+1e-14) %>% group_by(id) %>% summarise(E = sum(E))

    for (i in table_i_interval$id){
      time_to_trt_i = table_i_interval[table_i_interval$id==i,]$time_from_treat - 1
      time1_i = table_i_interval[table_i_interval$id==i,]$time1
      time2_i = table_i_interval[table_i_interval$id==i,]$time2
      if (time_to_trt_i >= 1){
        if (length(sigma_hat)>1){
          sum_B_hat = diag(sigma_hat^2)
          sum_B_true = diag(sigma_true^2)
        }else{
          sum_B_hat = sigma_hat^2
          sum_B_true = sigma_true^2
        }
        if (time_to_trt_i >= 2){
          for(k in 2:(time_to_trt_i)){
            sum_B_hat = sum_B_hat + multi_matrice_v2(B_hat,k-1,sigma_hat)
            sum_B_true = sum_B_true + multi_matrice_v2(B_true,k-1,sigma_true)
          }
        }
        correct = correct + sum_B_hat*(time2_i-time1_i)
        correct_true = correct_true+ sum_B_true*(time2_i-time1_i)
      }
      
    }
    matrix_correction = matrix(0,ncol = 2+nb_covar+nb_covar_bsln,nrow =2+nb_covar+nb_covar_bsln)
    matrix_correction_true = matrix(0,ncol = 2+nb_covar+nb_covar_bsln,nrow =2+nb_covar+nb_covar_bsln)
    matrix_correction[(2+nb_covar_bsln+1):(2+nb_covar+nb_covar_bsln),
                      (2+nb_covar_bsln+1):(2+nb_covar+nb_covar_bsln)] = correct
    matrix_correction_true[(2+nb_covar_bsln+1):(2+nb_covar+nb_covar_bsln),
                           (2+nb_covar_bsln+1):(2+nb_covar+nb_covar_bsln)] = correct_true
    

    if (sum(table_i_interval_E$E)>1){
      coef = try(solve(( t(cov_interval)%*%diag(rep(pas,dim(table_i_interval)[1])) %*%
                           cov_interval),
                       ( t(cov_interval)%*%table_i_interval_E$E)))
      
      coef_new = try(solve(( t(cov_interval)%*%diag(rep(pas,dim(table_i_interval)[1])) %*%
                               cov_interval) + matrix_correction,
                           ( t(cov_interval)%*%table_i_interval_E$E)))
      coef_new_true = try(solve(( t(cov_interval)%*%diag(rep(pas,dim(table_i_interval)[1])) %*%
                                    cov_interval) + matrix_correction_true,
                                ( t(cov_interval)%*%table_i_interval_E$E)))

    }
    else {
      coef = rep(0,nb_covar+2+nb_covar_bsln)
      coef_new = rep(0,nb_covar+2+nb_covar_bsln)
      coef_new_true = rep(0,nb_covar+2+nb_covar_bsln)

    }
    coefficients[c,] = coef
    coefficients_new[c,] = coef_new
    coefficients_new_true[c,] = coef_new_true
    c=c+1
  }
  
  return(list(coefficients=coefficients,coefficients_new=coefficients_new,coefficients_new_true=coefficients_new_true))
}


# Creation of a vector that counts since when the treatment started #
time_from_treat = function(Table_f_hat){
  #---------------------------------#
  # ARGUMENTS :                     #
  # Table_f_hat : dataframe         #
  #---------------------------------#
  
  Table_f_hat$time_from_treat = 0
  for (i in unique(Table_f_hat$id)){
    Table_f_hat_i = Table_f_hat[Table_f_hat$id==i,]
    cpt = sequence(rle(as.vector(Table_f_hat_i$id))$lengths)
    if (sum(Table_f_hat_i$B) != 0){
      Table_f_hat_i$time_from_treat = ((cpt -(min(which(Table_f_hat_i$B==1)))) * Table_f_hat_i$B + 1)
      Table_f_hat_i$tmp = ifelse(Table_f_hat_i$time1 %% 1 != 0 & Table_f_hat_i$B==1,1,0)
      Table_f_hat_i$time_from_treat = Table_f_hat_i$time_from_treat - cumsum(Table_f_hat_i$tmp)
      Table_f_hat_i = Table_f_hat_i[,-which(names(Table_f_hat_i)=="tmp")]
      Table_f_hat[Table_f_hat$id==i,] = Table_f_hat_i
    }
  }
  return(Table_f_hat)
}


# Function that returns  : B^nb * diag(sigma,...., sigma) * (B^nb)'
multi_matrice_v2 = function(B,nb,sigma){
  #----------------#
  # ARGUMENTS :    #
  # B : matrix     #
  # nb : integer   #
  # sigma : float  #
  #----------------#
  
  B = as.matrix(B)
  if (nrow(B)>1){
    mat_puis = diag(rep(1,nrow(B)))
    for(i in 1:nb){
      mat_puis = mat_puis%*%B
    }
    return(mat_puis%*%diag(sigma^2)%*%t(mat_puis))
  }else{
    return(as.matrix(sigma^2*B^(2*nb)))
  }
  
}



