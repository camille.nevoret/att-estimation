# ATT estimation

## Introduction

The codes presented in this project were used in the development of the paper "Debiasing the estimate of treatment effect on the treated with time-varying counfounders". 

Abstract : 

With the increased availability of large health databases comes the opportunity of evaluating treatment effect on new data sources. Through these databases time-dependent outcomes can be analysed as events that can be measured using counting processes. Estimating average treatment effect on the treated (ATT) requires modelling of time-varying covariate and time-dependent treatment and outcome. Gran et al. proposed an easy-to-implement method based on additive intensity regression to estimate ATT. We introduce a debiased estimate of the ATT based on a generalization of the Gran’s model for a potentially repeated outcome and in the presence of multiple time-dependent covariates and baseline covariates. Simulation analyses show that our corrected estimator outperforms Gran's uncorrected estimator. Our method is applied to intensive care real-life data from MIMIC-III databases to estimate vasoppressors effect on patients with sepsis. 


## File description  

- Example
	- **Data Simulation.Rmd** : Code to simulate longitudinal data with time-varying covariates, baseline covariates, treatment indicator and event indicator. A database with one line per patient and time interval is obtain.
	- **Final_Table.RData** : Saved database and saved VAR parameters
	- **ATT estimated.Rmd** : Code to model counterfactuals covariates and estimate ATT effect. Two graphs are proposed: trajectory of observed covariates and counterfactuals and the cumulative treatment effect.
- Functions
	- **Simulation_Vf_poisson.R** : Function to simulate longitudinal data. Its use is illustrated in the program "Data Simulation.Rmd".
	- **Counterfactual_Vf_poisson.R** : Function to model counterfactual using a VAR model. Its use is illustrated in the program "ATT estimated.Rmd".
	- **Estimated_Vf_poisson2.R** : Function to estimate ATT. Its use is illustrated in the program "ATT estimated.Rmd".